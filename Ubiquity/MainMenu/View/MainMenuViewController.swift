// Ubiquity
// Copyright © 2018 TST. All rights reserved.
// MainMenuViewController.swift 1/25/18

import CoreLocation
import GoogleMaps
import UIKit
import RealmSwift
import RxSwift

class MainMenuViewController: BaseViewController {
    
    @IBOutlet weak var adapterNameLabel: UILabel!
    
    var adapterName:String? {
        didSet { adapterNameLabel.text = adapterName! }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let adp = remoteData?.getDataAdapter() else { return }
        adapterName = String(describing: type(of: adp))
        remoteData?.addDelegate(for: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // TODO: test code - remove
    @IBAction func updateTapped(_ sender: Any) {
        let dict = ["lat": 38.052179, "lon":-81.108051]
        remoteData?.update(withDict: dict)
        
//        var mapObj = GMSMarker()
//        let location = CLLocationCoordinate2D(latitude: 38.052179, longitude: -81.108051)
//        mapObj.position = location
//        localData?.update(object: mapObj)
    }

    @IBAction func mapButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "map_segue", sender: self)
    }
}
