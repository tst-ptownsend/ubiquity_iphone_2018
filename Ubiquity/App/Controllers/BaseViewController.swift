//
//  BaseViewController.swift
//  Copyright © 2018 TST. All rights reserved.
//

import UIKit
import RxSwift

class BaseViewController: UIViewController, DataObserverDelegate {

    let remoteData = RemoteDataSingleton.getInstance()
    let localData = LocalDataSingleton.getInstance()
    let disposal = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        remoteData?.addDelegate(for: self)
        
        
    }
    
    deinit {  
        localData?.removeDelegate(for: self)
        remoteData?.removeDelegate(for: self)
    }
    
    // MARK: ObserverDelegate
    // overridden in subclass
    func dataUpdated(withDict dict: [AnyHashable : Any]) { }

}
