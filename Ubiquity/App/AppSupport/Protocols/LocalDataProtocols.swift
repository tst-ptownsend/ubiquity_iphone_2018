// Ubiquity
// Copyright © 2018 TST. All rights reserved.
// LocalDataProtocols.swift 1/30/18

import Foundation
import RealmSwift

protocol DataObjectProtocol {
    func getDataType() -> AnyObject.Type
    func update()
}

protocol LocalDataAdapterProtocol {
    func setDelegate(_ del:AdapterOwnerDelegate)
    func insert(object:DataObjectProtocol?)
    func update(object:DataObjectProtocol?)
    func delete(object:DataObjectProtocol?)
    func selectAll() -> [DataObjectProtocol]?
}
