//
//  RemoteDataProtocols.swift
//  Copyright © 2018 TST. All rights reserved.
//

import Foundation
import Swinject

// provides communication from data adapter singleton to VCs or any other client to respond to updates
protocol DataObserverDelegate:class {
    func dataUpdated(withDict dict:[AnyHashable:Any])
}

// provides callback from the adapter to the adapter singleton
protocol AdapterOwnerDelegate:class {
    func updateReceived(withData dict:[AnyHashable:Any])
}

// provides communication from the adapter singleton to its adapter
protocol RemoteDataAdapterProtocol {
    func setDelegate(_ del:AdapterOwnerDelegate)
    
    // database operations
    func insert(withDict dict:[AnyHashable:Any], intoTable:String?)
    func update(withDict dict:[AnyHashable:Any], table:String?)
    func delete(withDict dict:[AnyHashable:Any], fromTable:String?)
    func query(withPredicate pred:NSPredicate) -> [AnyHashable:Any]?
    func selectAll(from table:String?) -> [AnyHashable:Any]?
    
    // sets oberver to respond to data changes
    func setObserver()
}
