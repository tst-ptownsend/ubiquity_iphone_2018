//
//  Constants.swift
//  Copyright © 2018 TST. All rights reserved.
//

import Foundation

struct Constants {
    struct Data {
        static let LOCAL_DATABASE_PLATFORM:LocalDatabaseType = .realm
        static let DATABASE_PLATFORM:RemoteDatabaseType = RemoteDatabaseType.firebase
        static let DATABASE_ENDPOINT = ""
        
        struct Parse {
            static let APPLICATION_ID = "map-test"
            static let CLIENT_KEY = "map-test-clientkey-1234"
            static let ENDPOINT = "wss://tst-parse.herokuapp.com/parse"
            static let SERVER = "https://tst-parse.herokuapp.com/parse/"
        }
    }
    struct Gee {
//        static let GEE_BASE_URL = "https://earth.t-sciences.com/2d_blue_marble-v001_ssl/query?request=" //ImageryMaps&channel=1006&version=1&x=1&y=1&z=4&is2d=t"
        static let GEE_BASE_URL = "https://ispatial-geep-2.corp.t-sciences.com/SF_2d_Merc-v001/query?request="
    }
    struct Map {
        static let GOOGLE_MAPS_KEY = "AIzaSyDNSlS-64KLmtPCvIqyIXgKOFxUe842jHc"
    }
}
