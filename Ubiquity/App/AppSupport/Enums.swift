//
//  Enums.swift
//  Ubiquity
//
//  Created by Phil Townsend on 1/22/18.
//  Copyright © 2018 TST. All rights reserved.
//

import Foundation

enum LocalDatabaseType {
    case realm
}

enum RemoteDatabaseType {
    case firebase
    case parse
}

enum FeatureCreationMode {
    case point
    case line 
    case polygon
}
