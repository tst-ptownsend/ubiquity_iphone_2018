// Ubiquity
// Copyright © 2018 TST. All rights reserved.
// Extensions.swift 1/25/18

import Foundation
import RealmSwift
import UIKit

extension NSURLRequest {
    static func allowsAnyHTTPSCertificateForHost(host: String) -> Bool {
        return true
    }
}

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for result in self {
            if let result = result as? T {
                array.append(result)
            }
        }
        return array
    }
}

extension UITextField {
    func setPadding(leftAmount:CGFloat?, rightAmount:CGFloat?) {
        if leftAmount != nil {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: leftAmount!, height: self.frame.size.height))
            self.leftView = paddingView
            self.leftViewMode = .always
        }
        if rightAmount != nil {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: rightAmount!, height: self.frame.size.height))
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
}
