//
//  BaseDataAdapter.swift
//  Copyright © 2018 TST. All rights reserved.
//

import UIKit

class BaseDataAdapter: NSObject, RemoteDataAdapterProtocol {
    
    weak var delegate:AdapterOwnerDelegate?
    
    func setDelegate(_ del: AdapterOwnerDelegate) {
        self.delegate = del
    }
    
    func insert(withDict dict:[AnyHashable:Any], intoTable:String?) { }
    func update(withDict dict:[AnyHashable:Any], table:String?) { }
    func delete(withDict dict:[AnyHashable:Any], fromTable:String?) { }
    func setObserver() { }
    func query(withPredicate pred:NSPredicate) -> [AnyHashable:Any]? { return nil }
    func selectAll(from table:String?) -> [AnyHashable:Any]? { return nil }
}
