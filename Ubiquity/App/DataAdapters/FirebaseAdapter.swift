//
//  FirebaseAdapter.swift
//  Copyright © 2018 TST. All rights reserved.
//

import UIKit
import CoreLocation
import FirebaseCore
import FirebaseDatabase

class FirebaseAdapter: BaseDataAdapter {
    
    private var dbRef:DatabaseReference?
    
    override init() {
        super.init()
        dbRef = Database.database().reference()
    }
    
    override func setObserver() {
        dbRef?.observe(.value, with: { (snap: DataSnapshot!) in 
            let updateDict = snap?.value as! [String:AnyObject]
            self.delegate?.updateReceived(withData: updateDict)
        })
    } 
    
    override func update(withDict dict: [AnyHashable : Any], table: String?) {
        let speed = 55.0
        guard let data = dict as? [String:Any] else { return }
        
        let myGuid = "e71ec0fe-7829-4952-a95a-8ba6b1d15378"
        if let childRef = dbRef?.child("tracking").child(myGuid) as? DatabaseReference {
            childRef.child("lon").setValue(data["lon"])
            childRef.child("lat").setValue(data["lat"])
            childRef.child("updated").setValue(Int(Date().timeIntervalSince1970 * 1000))
            childRef.child("speed").setValue((speed as Double) * 2.23694)
        }
    }
}
