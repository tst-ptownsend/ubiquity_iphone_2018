// Ubiquity
// Copyright © 2018 TST. All rights reserved.
// RealmAdapter.swift 1/24/18

import UIKit
import Realm
import RealmSwift

class RealmAdapter:LocalDataAdapterProtocol {
    
    weak var delegate:AdapterOwnerDelegate?
    
    func setDelegate(_ del: AdapterOwnerDelegate) {
        self.delegate = del 
    }
    
    func insert(object: DataObjectProtocol?) {
        print(object?.getDataType() as Any)
    }
    
    func update(object: DataObjectProtocol?) {
        print(object?.getDataType() as Any)
    }
    
    func delete(object: DataObjectProtocol?) {
        print(object?.getDataType() as Any)
    }
    
    func selectAll() -> [DataObjectProtocol]? {
        return nil
    }
    
}
