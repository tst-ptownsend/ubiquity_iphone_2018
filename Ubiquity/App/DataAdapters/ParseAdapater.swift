//
//  ParseDataAdapater.swift
//  Copyright © 2018 TST. All rights reserved.
//

import UIKit
import Parse 
import ParseLiveQuery

class ParseAdapater: BaseDataAdapter {
    
    private var subscription:Subscription<PFObject>!
    let queryClient:Client = Client(server: Constants.Data.Parse.ENDPOINT)
    
    override func update(withDict dict: [AnyHashable : Any], table:String?) {
        let tableName = (table != nil) ? table : "Tracking"
        let deviceToken = UIDevice.current.identifierForVendor?.uuidString
        let query = PFQuery(className: tableName!).whereKey("deviceToken", equalTo: deviceToken)
        try! query.findObjectsInBackground(block: { (objs, err) -> () in 
            let trackLocation = PFObject(className: "Tracking")
            let latitude = dict["lat"]
            let longitude = dict["lon"]
            let timeStamp = Int(Date().timeIntervalSince1970 * 1000)
            
            if objs != nil && (objs?.count)! > 0 {
                // update
                if let queryObj = objs![0] as? PFObject {
                    queryObj.setValue(latitude, forKey: "lat")
                    queryObj.setValue(longitude, forKey: "lon")
                    queryObj.setValue(timeStamp, forKey: "updated")
                    try! queryObj.save()
                }
            } else {
                // insert 
                trackLocation.setValue("ptownsend", forKey: "username")
                trackLocation.setValue(latitude!, forKey: "lat")
                trackLocation.setValue(longitude!, forKey: "lon")
                trackLocation.setValue(deviceToken, forKey: "deviceToken")
                trackLocation.setValue(timeStamp, forKey: "updated")
                trackLocation.saveInBackground(block: { (success, error) -> Void in 
                    if success {
                        print("SUCCESS = \(success)")
                    } else {
                        print(error.debugDescription)
                    }
                })
            }
        })
    }
    
    override func setObserver() {
        print("SETTING PARSE OBSERVER")
        let _query = PFQuery(className: "Tracking").whereKeyExists("deviceToken")
        self.subscription = self.queryClient.subscribe(_query).handle(Event.updated) { _, trackObj in 
            if let track = trackObj as? PFObject {
                var dict = [String:Any]()
                if let lat = (track.object(forKey: "lat")!) as? Double {
                    dict["lat"] = lat
                }
                if let lon = (track.object(forKey: "lon")!) as? Double {
                    dict["lon"] = lon
                }
                if let deviceToken = (track.object(forKey: "deviceToken")!) as? String {
                    dict["deviceToken"] = deviceToken
                }
                DispatchQueue.main.async { self.delegate?.updateReceived(withData: dict) }
            }
        }
    }
    
}
