//  Ubiquity
//  DataAdapterSingleton.swift
//  Copyright © 2018 TST. All rights reserved.
//

import Foundation
import Parse 
import Swinject
import SwiftyJSON

class RemoteDataSingleton: NSObject, AdapterOwnerDelegate {
    
    private lazy var delegates = MulticastDelegate<DataObserverDelegate>(strongReferences: false)
    private var dependencies:Container?
    
    // MARK: Singleton Initialization
    
    static var sharedInstance:RemoteDataSingleton? = nil 
    static func getInstance() -> RemoteDataSingleton? {
        if sharedInstance == nil {
            let cnt = Container()
            var _adapter:RemoteDataAdapterProtocol?
            switch Constants.Data.DATABASE_PLATFORM {
                case .firebase: _adapter = FirebaseAdapter()
                case .parse: _adapter = ParseAdapater()
            }
            _adapter?.setObserver()
            
            // register/inject adapter type
            cnt.register(RemoteDataAdapterProtocol.self) { _ in _adapter! }
            sharedInstance = RemoteDataSingleton(withContainer: cnt)
        }
        return sharedInstance
    }
    
    // MARK: Constructors
    // init family is private only singleton instance is possible
    
    private convenience init(withContainer cnt:Container) {
        self.init()
        dependencies = cnt 
        let adapter = dependencies?.resolve(RemoteDataAdapterProtocol.self)
        adapter?.setDelegate(self)
    } 
    
    private override init() {
        super.init()
    }
    
    // MARK: Dependencies
    
    func getDataAdapter() -> RemoteDataAdapterProtocol? {
        guard let dep = dependencies?.resolve(RemoteDataAdapterProtocol.self) else { return nil }
        return dep
    }
    
    // MARK: Database Operations
    
    func update(withDict dict:[String:Any]) {
        guard let adapter = getDataAdapter() else { return }
        adapter.update(withDict: dict, table: nil)
    }
    
    // MARK: Multicast Delegate
    
    func addDelegate(for sender:DataObserverDelegate) {
        delegates.addDelegate(sender)
    }
    
    func removeDelegate(for sender:DataObserverDelegate) {
        if delegates.containsDelegate(sender) {
            delegates.removeDelegate(sender)
        }
    }
    
    // MARK: AdapterOwnerDelegate
    
    func updateReceived(withData dict: [AnyHashable : Any]) {
        delegates.invokeDelegates({ delegate in
            delegate.dataUpdated(withDict: dict)
        })
    }
}
