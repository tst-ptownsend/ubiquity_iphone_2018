// Ubiquity
// Copyright © 2018 TST. All rights reserved.
// LocalDataSingleton 1/25/18

import Foundation
import Parse 
import RealmSwift
import Swinject
import SwiftyJSON

class LocalDataSingleton: NSObject, AdapterOwnerDelegate  {
    
    private lazy var delegates = MulticastDelegate<DataObserverDelegate>(strongReferences: false)
    private var dependencies:Container?
    
    // MARK: Singleton Initialization
    
    static var sharedInstance:LocalDataSingleton? = nil
    static func getInstance() -> LocalDataSingleton? {
        if sharedInstance == nil {
            let cnt = Container()
            var _adapter:LocalDataAdapterProtocol? 
            
            // will be extended as necessary when local database types are added
            switch Constants.Data.LOCAL_DATABASE_PLATFORM {
            case .realm: _adapter = RealmAdapter()
            }
            
            // register/inject adapter type
            cnt.register(LocalDataAdapterProtocol.self) { _ in _adapter! }
            sharedInstance = LocalDataSingleton(withContainer: cnt)
        }
        return sharedInstance
    }
    
    // MARK: Constructors
    // init family is private only singleton instance is possible
    
    private convenience init(withContainer cnt:Container) {
        self.init()
        dependencies = cnt 
        let adapter = dependencies?.resolve(RemoteDataAdapterProtocol.self)
        adapter?.setDelegate(self)
    } 
    
    private override init() {
        super.init()
    }
    
    func update(object:DataObjectProtocol) {
        
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(object as! Object)
        }
    }
    
    // MARK: Multicast Delegate
    
    func addDelegate(for sender:DataObserverDelegate) {
        delegates.addDelegate(sender)
    }
    
    func removeDelegate(for sender:DataObserverDelegate) {
        if delegates.containsDelegate(sender) {
            delegates.removeDelegate(sender)
        }
    }
    
    // MARK: AdapterOwnerDelegate
    
    func updateReceived(withData dict: [AnyHashable : Any]) {
        delegates.invokeDelegates({delegate in 
            delegate.dataUpdated(withDict: dict)
        })
    }
}
