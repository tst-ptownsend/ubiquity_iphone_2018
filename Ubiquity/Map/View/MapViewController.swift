// Ubiquity
// Copyright © 2018 TST. All rights reserved.
// MapViewController.swift 1/25/18

import UIKit
import CoreLocation
import GoogleMaps
import RxSwift

class MapViewController: BaseViewController {
    
    private let locationManager = CLLocationManager()
    lazy var model = MapViewModel()
    
    lazy var line = GMSPolyline()
    lazy var polygon = GMSPolygon()
    lazy var marker = GMSMarker()
    lazy var path = GMSMutablePath()
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var editViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var geeUrlEditView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = model 
        marker.map = mapView
        model.mapView = mapView
        model.mode = .line 
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        setObservers()
        
        mapView?.animate(toLocation: CLLocationCoordinate2D(latitude: 36.687185, longitude: -111.733720))
        mapView.animate(toZoom: 7.5)
        
        let kmlUrlString = "https://developers.google.com/maps/documentation/javascript/examples/kml/westcampus.kml"
        //let kmlUrlString = "https://google-developers.appspot.com/maps/documentation/javascript/examples/full/layer-kml"
        model.loadKml(from: kmlUrlString)
        model.loadKml(fromFile: "AfternoonRide")
        
        editViewConstraint.constant = geeUrlEditView.frame.size.height
        geeUrlEditView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
    }
    
    @IBAction func goButtonTapped(_ sender: Any) {
        UIView.animate(withDuration: 0.5, animations: {
            self.editViewConstraint.constant = self.geeUrlEditView.frame.size.height
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func swapTapped(_ sender: Any) {
        //model.swap()
        UIView.animate(withDuration: 0.5, animations: {
            self.editViewConstraint.constant = 25
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func saveFeatureTapped(_ sender: Any) {
        model.save()
    }
    
    private func setObservers() {
        model.tapLocation.asObservable().subscribe(onNext: { location in
            switch self.model.mode! {
            case .point: self.marker.position = (location.count > 0) ? location.last! : self.marker.position
            case .line:
                if let vertex = location.last {
                    self.path.add(vertex)
                    self.line.path = self.path 
                    self.line.map = self.mapView
                }
            case .polygon:
                if let vertex = location.last {
                    self.path.add(vertex)
                    self.polygon.path = self.path 
                    self.polygon.map = self.mapView
                }
            }
            
        }).disposed(by: disposal)
        
        model.allFeatures.asObservable().subscribe(onNext:{ features in 
            
        })
    }

}
