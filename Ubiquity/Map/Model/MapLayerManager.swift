// Ubiquity
// Copyright © 2018 TST. All rights reserved.
// MapLayerManager.swift 2/5/18


import UIKit
import GoogleMaps

class MapLayerManager: NSObject {
    
    var mapView:GMSMapView?
    
    internal override init() {
        super.init()
    }
    
    convenience init(withMap map:GMSMapView?) {
        self.init()
        mapView = map
    }
    
    func initializeGeeTiles() {
        let urlInfo:GMSTileURLConstructor = { (x: UInt, y: UInt, zoom: UInt) -> URL in 
            let baseUrl = Constants.Gee.GEE_BASE_URL
//            let tileUrl = "\(baseUrl)ImageryMaps&channel=1006&version=1&x=\(x)&y=\(y)&z=\(zoom)&is2d=t"
            
            let tileUrl = "\(baseUrl)ImageryMaps&channel=1005&version=1&x=\(x)&y=\(y)&z=\(zoom)&is2d=t"
            
            let resolvedIP = Utilities.getIPAddress(fromUrlString: "earth.t-sciences.com")!
            print("NUMERIC ADDRESS: \(resolvedIP)")
            
            return URL(string: tileUrl)!
        }
        
        let layer = GMSURLTileLayer(urlConstructor: urlInfo)
        if mapView != nil {
            DispatchQueue.main.async {
                layer.zIndex = 2
                layer.tileSize = 512
                layer.map = self.mapView
            }
        }
    }
    
}

