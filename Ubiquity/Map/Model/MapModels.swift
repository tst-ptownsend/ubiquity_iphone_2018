// Ubiquity
// Copyright © 2018 TST. All rights reserved.
// MapModels.swift 1/26/18

import CoreLocation
import Foundation
import GoogleMaps
import RealmSwift
import Realm

/* Realm specific map models */

class MapCoordinate:Object {
    @objc dynamic var latitude:Double = 0.0
    @objc dynamic var longitude:Double = 0.0
}

class MapTile: Object {
    @objc dynamic var tile:NSData? = nil 
}

class Circle:Object {
    @objc dynamic var radius:Double = 0.0
    let center = MapCoordinate()
}

class Point:Shape {
}

class Polygon:Shape {
}

class Polyline:Shape {
}

class Shape:Object {
    @objc dynamic var position:MapCoordinate? // for shapes other than points, this shuould be the centroid
    @objc dynamic var featureId:String = UUID().uuidString
    var path = List<MapCoordinate>()
    
    func setPath(withGMSPath path:GMSPath) {
        let _path = List<MapCoordinate>()
        if Int(path.count()) > 0 {
            for index in 0..<(path.count()) {
                let coordinate = MapCoordinate()
                let vertex = path.coordinate(at: UInt(index))
                coordinate.latitude = vertex.latitude
                coordinate.longitude = vertex.longitude 
                _path.append(coordinate)
            }
        }
        if _path.count > 0 {
            self.path = _path
        }
    }
}
