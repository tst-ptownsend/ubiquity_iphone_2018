// Ubiquity
// Copyright © 2018 TST. All rights reserved.
// MapExtensions.swift 1/30/18

import CoreLocation
import Foundation
import GoogleMaps
import RealmSwift

protocol MapFeatureProtocol {
    func updateFeature(withIdentifier featureId:String)
    func update(withCoordinate:CLLocationCoordinate2D, persist:Bool?)
    func getDataObject() -> DataObjectProtocol?
}

extension MapViewController:CLLocationManagerDelegate,  GMSMapViewDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else { return }
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true 
    }
}

extension Object:DataObjectProtocol { 
    func getDataType() -> AnyObject.Type {
        return type(of: self)
    }
    func update() {
        let realm = try! Realm()
        try! realm.write {
            realm.add(self)
        }
    }
}

extension GMSOverlay:MapFeatureProtocol {
    private func getPathVertices(forFeature feature:GMSOverlay) -> List<MapCoordinate>? {
        var objPath:List<MapCoordinate>?
        var path:GMSPath?
        if let line = feature as? GMSPolyline {
            path = line.path
        } else if let poly = feature as? GMSPolygon {
            path = poly.path
        }
        
        for index in 0..<(path?.count())! {
            let coordinate = MapCoordinate()
            let vertex = path?.coordinate(at: UInt(index))
            coordinate.latitude = (vertex?.latitude)!
            coordinate.longitude = (vertex?.longitude)! 
            objPath?.append(coordinate)
        }
        return objPath
    }
    func getDataObject() -> DataObjectProtocol? {
        // TODO: expand this as necessary to allow addition of data object from other platforms. Realm is default
        
        // Realm Conversion
        if let point = self as? GMSMarker {
            let position = MapCoordinate()
            position.latitude = point.position.latitude
            position.longitude = point.position.longitude
            let feature = Point()
            feature.position = position
            return feature 
        }
        else {
            var feature:Shape?
            if let line = self as? GMSPolyline {
                let feature = Polyline()
                feature.path = getPathVertices(forFeature: line)!
            } 
            else if let poly = self as? GMSPolygon {
                let feature = Polygon()
                feature.path = getPathVertices(forFeature: poly)!
            }
            return feature
        }
    }
    
    func updateFeature(withIdentifier featureId:String) { }
    
    func update(withCoordinate: CLLocationCoordinate2D, persist:Bool?) {
        if persist! {
            if let realmObject = getDataObject() as? Object {
                let realm = try! Realm()
                try! realm.write { realm.add(realmObject) }
            }
        }
    }
}

