// Ubiquity
// Copyright © 2018 TST. All rights reserved.
// MapViewModel.swift 1/25/18

import Foundation
import CoreLocation
import GoogleMaps
import RealmSwift
import RxSwift

public final class MapViewModel:NSObject, GMSMapViewDelegate  {
    
    // TODO: remove this test var and associations
    lazy var testBounds = [CLLocationCoordinate2D]()
    var locationIndex:Bool = false 
    
    // Realm observable properties
    var tapLocation:Variable<[CLLocationCoordinate2D]> = Variable([])
    var allFeatures:Variable<[Object]> = Variable([])
    let disposal = DisposeBag()
    
    var realm:Realm?
    var mode:FeatureCreationMode?
    
    var mapView:GMSMapView? {
        didSet {
            layerManager = MapLayerManager(withMap: mapView)
            layerManager?.initializeGeeTiles()
        }
    }
    
    lazy var path = GMSMutablePath()
    var kmlUrlParser:GMUKMLParser?
    var kmlFileParser:GMUKMLParser?
    
    var layerManager:MapLayerManager?
    
    // MARK: GMSMapViewDelegate
    
//    convenience init(withMap map:GMSMapView?, andLayerManager mgr:MapLayerManager?) {
//        self.init()
//        realm = try! Realm()
//        mapView = map
//        
//        layerManager = mgr
//        layerManager?.initializeGeeTiles()
//    }
    
    internal override init() {
        super.init()
        realm = try! Realm()
    }
    
    func swap() {
        locationIndex = !locationIndex
        let mapPosition = testBounds[locationIndex ? 1 : 0]
        mapView?.animate(to: GMSCameraPosition(target: mapPosition, zoom: 11, bearing: 0, viewingAngle: 0))
    }
    
    func loadKml(fromFile file:String) {
        if kmlFileParser == nil {
            let path = Bundle.main.path(forResource: file, ofType: "kml")
            let url = URL.init(fileURLWithPath: path!)
            kmlFileParser = GMUKMLParser(url: url)
            kmlFileParser?.parse()
            
            for item:GMUGeometryContainer in (kmlFileParser?.placemarks)! {
                if let _item = item.geometry as? GMULineString {
                    let line = GMSPolyline(path: _item.path)
                    line.zIndex = 20
                    line.strokeColor = UIColor.yellow
                    line.map = mapView
                    
                    let bounds = GMSCoordinateBounds(path: line.path!)
                    testBounds.append(GMSGeometryInterpolate(bounds.northEast, bounds.southWest, -0.5))
                }
            }
            
            let renderer = GMUGeometryRenderer(map: mapView!, geometries: (kmlFileParser?.placemarks)!, styles: kmlFileParser?.styles)
            renderer.render()
        }
    }
    
    func loadKml(from url:String) {
        if kmlUrlParser == nil {
            kmlUrlParser = GMUKMLParser(url: URL(string: url)!)
            kmlUrlParser?.parse()
            
            var cBounds = GMSCoordinateBounds()
            for item:GMUGeometryContainer in (kmlUrlParser?.placemarks)! {
                if let point = item.geometry as? GMUPoint {
                    testBounds.append(point.coordinate)
                    break
                }
            }
            
            let renderer = GMUGeometryRenderer(map: mapView!, geometries: (kmlUrlParser?.placemarks)!, styles: kmlUrlParser?.styles)
            renderer.render()
        }
    }
    
    func save() {
        let pathCount:Int = Int(path.count())
        var feature:Shape?
        switch(mode!) {
            case .point: 
                feature = Point()
                let mapCoord = MapCoordinate()
                if pathCount > 0 {
                    mapCoord.latitude = path.coordinate(at: UInt(pathCount - 1)).latitude
                    mapCoord.longitude = path.coordinate(at: UInt(pathCount - 1)).longitude
                    feature?.position = mapCoord
                }
            case .line: 
                feature = Polyline()
                feature?.setPath(withGMSPath: path)
            case .polygon: 
                feature = Polygon()
                feature?.setPath(withGMSPath: path)
        }
        feature?.update()
        clearMap()
    }
    
    private func clearMap() {
        mapView?.clear()
        path.removeAllCoordinates()
    }
    
    public func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        // value is observed in VC: event raised when tapLocation changes
        tapLocation.value.append(coordinate)
        if mode == .line || mode == .polygon {
            path.add(coordinate)
        }
    }
}
