//
//  LoginViewController.swift
//  Copyright © 2018 TST. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        performSegue(withIdentifier: "login_segue", sender: self)
    }
    
}
