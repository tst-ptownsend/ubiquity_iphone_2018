//
//  AppDelegate.swift
//  Ubiquity
//
//  Created by Phil Townsend on 2/25/18.
//  Copyright © 2018 TST. All rights reserved.
//

import UIKit
import Parse 
import FirebaseCore
import FirebaseDatabase
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    // configuration placed here because Parse needs configured before DataAdapterSingleton initialization 
    private func configureDatabase(for dbType:RemoteDatabaseType) {
        switch dbType {
        case .firebase: FirebaseApp.configure()
        case .parse:
            let parseConfig = ParseClientConfiguration(block: { (clientConfig) in 
                clientConfig.applicationId = Constants.Data.Parse.APPLICATION_ID
                clientConfig.clientKey = Constants.Data.Parse.CLIENT_KEY
                clientConfig.server = Constants.Data.Parse.SERVER
            })
            Parse.initialize(with: parseConfig)
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        configureDatabase(for: Constants.Data.DATABASE_PLATFORM)
        GMSServices.provideAPIKey(Constants.Map.GOOGLE_MAPS_KEY)
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
}

